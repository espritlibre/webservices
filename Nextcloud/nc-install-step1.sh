#!/bin/bash

# Script to launch on Nextcloud server

function main() {
    readvars "$@"

    download

    extract

    mysql_prepare

    letsencrypt

    apache2_prepare

    nextcloud_prepare

    cron_nagios_prog

    nextcloud_finalize

    borg_prepare
}

function readvars() {
    BKPPORT=22
    VERSION=latest

    while [[ $# -gt 0 ]]
    do
    key="$1"

    case $key in
        -d|--domain)
            if [ -w "$2" ]; then
                DOMAIN="${2}"
            else
                echo "[ERROR] Invalid domain : $2"
                exit 1
            fi
            shift
            ;;
        -h|--host)
            if [ -w "$2" ]; then
                SERVERNAME="${2}"
            else
                echo "[WARNING] No server name to configure backup : $2"
            fi
            shift
            ;;
        -dp|--dpass)
            if [ -w "$2" ]; then
                DBPASS="${2}"
            else
                echo "[ERROR] No password to configure database : $2"
                exit 1
            fi
            shift
            ;;
        -v|--version)
            if [ -w "$2" ]; then
                VERSION="${2}"
            fi
            shift
            ;;
        -b|--backup)
            if [ -w "$2" ]; then
                BKPSERVER="${2}"
            else
                echo "[WARNING] No backup server name to configure backup : $2"
            fi
            shift
            ;;
        -p|--port)
            if [ -w "$2" ]; then
                BKPPORT="${2}"
            else
                echo "[WARNING] No backup server port to configure backup : $2"
            fi
            shift
            ;;
        -bp|--bpass)
            if [ -w "$2" ]; then
                BORGPASS="${2}"
            else
                echo "[WARNING] No passphrase to configure backup : $2"
            fi
            shift
            ;;
        *)
            echo "[WARNING] Unkown option: $1"
            ;;
    esac
    shift
    done

	if [ -z "${DOMAIN}" ]; then
		echo '[ERROR] No valid domain.'
		exit 1
    else
        CLEANDOMAIN=$(echo ${DOMAIN} | sed 's,https*://,,g' | sed 's,/,,g' | sed 's,\.,,g' | sed 's,-,,g')
	fi

    if [ -z "${DBPASS}" ]; then
		echo '[ERROR] No valid database password.'
		exit 1
	fi

    return 0
}

function download() {
    # nommage des versions : `nextcloud-18.0.4.tar.bz2` ou `latest-18.tar.bz2` ou `latest.tar.bz2`
    if [ "${#VERSION}" = 2 ]; then
        VERSION="latest-${VERSION}"
    elif [ "${VERSION}" != "latest" ]; then
        VERSION="nextcloud-${VERSION}"
    fi
    wget -q --show-progress https://download.nextcloud.com/server/releases/${VERSION}.tar.bz2
}

function extract() {
    mkdir nextcloud
    tar xjf ${VERSION}.tar.bz2 -C "nextcloud"
    chown -R root:www-data nextcloud
    chmod -R o-rwx nextcloud
    chown -R www-data: nextcloud/apps
    chown -R www-data: nextcloud/config
    mkdir -p /data/cloud.${DOMAIN}/donnees
    chown root:www-data /data/cloud.${DOMAIN}
    chown www-data: /data/cloud.${DOMAIN}/donnees
    chmod -R o-rwx /data/cloud.${DOMAIN}
    mv nextcloud /data/cloud.${DOMAIN}/www
}

function mysql_prepare() {
    mysql -e "CREATE DATABASE nc_${CLEANDOMAIN}"
    mysql -e "CREATE USER '${CLEANDOMAIN}'@'localhost' identified by '${DBPASS}';"
    mysql -e "GRANT ALL ON nc_${CLEANDOMAIN}.* to '${CLEANDOMAIN}'@'localhost';"
}

function letsencrypt() {
    cp -a /etc/letsencrypt/configs/letsencrypt_nextcloud.conf /etc/letsencrypt/configs/${DOMAIN}.conf
    sed -i "s/domaine.fr/${DOMAIN}/g" /etc/letsencrypt/configs/${DOMAIN}.conf
    # l'option --dry-run permet de tester avant de générer le certificat TLS
    # Let's Encrypt bloque quand on fait trop de tentatives ou de renouvellement sans cette option
    # or il arrive que certains clients n'ont pas fait la déclaration DNS ou l'ont mal faite
    # certbot certonly --dry-run -c /etc/letsencrypt/configs/${DOMAIN}.conf
    certbot certonly -c /etc/letsencrypt/configs/${DOMAIN}.conf
}

function apache2_prepare() {
    cp -a /etc/apache2/sites-available/apache2_nextcloud.conf /etc/apache2/sites-available/${DOMAIN}.conf
    sed -i "s/domaine.fr/${DOMAIN}/g" /etc/apache2/sites-available/${DOMAIN}.conf
    if [ -e "/etc/letsencrypt/live/${DOMAIN}/cert.pem" ]; then
        ln -s /etc/apache2/sites-available/${DOMAIN}.conf /etc/apache2/sites-enabled/${DOMAIN}.conf
        apache2ctl -t # facultatif, teste la configuration d'Apache
        systemctl reload apache2
    else
        echo "Domain not declared in DNS? Please check before activating in apache!"
        echo "When ready, launch: certbot certonly -c /etc/letsencrypt/configs/${DOMAIN}.conf"
        echo "then: ln -s /etc/apache2/sites-available/${DOMAIN}.conf /etc/apache2/sites-enabled/${DOMAIN}.conf"
        echo "and finally: restart apache: systemctl reload apache2"
    fi
}

function nextcloud_prepare() {
    sudo -u www-data php -f /data/cloud.${DOMAIN}/www/occ db:add-missing-indices
    sudo -u www-data php -f /data/cloud.${DOMAIN}/www/occ db:convert-filecache-bigint
}

function cron_nagios_prog() {
    echo "*/5  *  *  *  * www-data        php -f /data/cloud.${DOMAIN}/www/cron.php" >> /etc/cron.d/nextcloud
    # Mettre des heures aléatoires entre 22h et 6h...
    echo "0    6  *  *  1 www-data        php -f /data/cloud.${DOMAIN}/www/occ app:update --all" >> /etc/cron.d/nextcloud
    # Mettre un jour et des heures aléatoires entre 22h et 6h...
    echo " 40  2  7  *  * root    certbot certonly -c /etc/letsencrypt/configs/${DOMAIN}.conf" >> /etc/cron.d/certbot
    # Mettre des heures aléatoires entre 22h et 6h...
    echo "  0  6  1  *  * root    /usr/local/bin/space_usage.sh -m support@lydra.fr -n -s -p /data/cloud.${DOMAIN}/donnees > /dev/null 2>&1" >> /etc/cron.d/spaceusage

    echo "command[check_ssl_cert_${CLEANDOMAIN}]=/usr/bin/sudo /usr/lib/nagios/plugins/check_ssl_cert -H localhost -w 30 -c 15 -f /etc/letsencrypt/live/cloud.${DOMAIN}/cert.pem" >> /etc/nagios/nrpe_local.cfg
    systemctl restart nagios-nrpe-server
}

function nextcloud_finalize() {
    sed -i "s+^);+  'forcessl' => true,\n&+g" /data/cloud.${DOMAIN}/www/config/config.php
    sed -i "s+^);+  'default_language' => 'fr',\n&+g" /data/cloud.${DOMAIN}/www/config/config.php
    sed -i "s+^);+  'logtimezone' => 'Europe/Paris',\n&+g" /data/cloud.${DOMAIN}/www/config/config.php
    sed -i "s+^);+  'memcache.local' => '\\\\OC\\\\Memcache\\\\APCu',\n&+g" /data/cloud.${DOMAIN}/www/config/config.php
    sed -i "s+^);+  'trashbin_retention_obligation' => '31, auto',\n&+g" /data/cloud.${DOMAIN}/www/config/config.php
    sed -i "s+^);+  'versions_retention_obligation' => 'auto, 92',\n&+g" /data/cloud.${DOMAIN}/www/config/config.php
    sed -i "s+^);+  'activity_expire_days' => 365,\n&+g" /data/cloud.${DOMAIN}/www/config/config.php
}

function borg_prepare() {
    echo "${BORGPASS}" > /root/.borg/${CLEANDOMAIN}
    echo "${DBPASS}" > /root/.borg/mysql_${CLEANDOMAIN}
    chmod go-rwx /root/.borg/*
    ssh-keygen -q -t rsa -f /root/.ssh/${CLEANDOMAIN} -C "${SERVERNAME}" -N ""

    echo "Host ${CLEANDOMAIN}" >> /root/.ssh/config
    echo "        HostName        ${BKPSERVER}" >> /root/.ssh/config
    echo "        Port            ${BKPPORT}" >> /root/.ssh/config
    echo "        User            ${CLEANDOMAIN}" >> /root/.ssh/config
    echo "        PasswordAuthentication  no" >> /root/.ssh/config
    echo "        IdentityFile    /root/.ssh/${CLEANDOMAIN}" >> /root/.ssh/config
    echo "        IdentitiesOnly  yes" >> /root/.ssh/config

    echo "RSA key to add on Borgbackup server is:"
    more /root/.ssh/${CLEANDOMAIN}.pub
}

main "$@"

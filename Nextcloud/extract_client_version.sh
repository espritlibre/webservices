#!/bin/bash
FILES=$1

# Read standard apache2 or nginx logs and extract
# - nextcloud-client version
# - operating system of the user
# - username
# Output group occurrences and sort it by "client version;OS;username"
sed -n 's#^.*- \(.*\) \[\(.*\)/202[0-9]:.*(\(.*\)) mirall/\([0-9]*.[0-9]*.[0-9]*\).*$#\2 \4;\3;\1#p' $FILES > $FILES.sort

for b in $(awk '{print $1}' $FILES.sort|sort|uniq)
do echo $b
grep $b $FILES.sort|awk '{print $2}'|sort|uniq
done

rm $FILES.sort

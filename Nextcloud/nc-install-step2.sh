#!/bin/bash

# Script to launch on Borgbackup server

function main() {
    readvars "$@"

    prepare_env

    supervision

    rsa_key
}

function readvars() {
    while [[ $# -gt 0 ]]
    do
    key="$1"

    case $key in
        -d|--domain)
            if [ -w "$2" ]; then
                DOMAIN="${2}"
            else
                echo "[ERROR] Invalid domain : $2"
                exit 1
            fi
            shift
            ;;
        -h|--host)
            if [ -w "$2" ]; then
                SERVERNAME="${2}"
            else
                echo "[WARNING] No server name to configure backup : $2"
            fi
            shift
            ;;
        *)
            echo "[WARNING] Unkown option: $1"
            ;;
    esac
    shift
    done

	if [ -z "${DOMAIN}" ]; then
		echo '[ERROR] No valid domain.'
		exit 1
    else
        CLEANDOMAIN=$(echo ${DOMAIN} | sed 's,https*://,,g' | sed 's,/,,g' | sed 's,\.,,g' | sed 's,-,,g')
	fi

    return 0
}

function prepare_env() {
    useradd -g backupeurs -m -d /data/${CLEANDOMAIN} ${CLEANDOMAIN}
    mkdir /data/${CLEANDOMAIN}/.ssh
    touch /data/${CLEANDOMAIN}/.ssh/authorized_keys
    mkdir -p /data/${CLEANDOMAIN}/borg/$(echo ${SERVERNAME} | sed 's+\..*++g')
    chown -R ${CLEANDOMAIN}: /data/${CLEANDOMAIN}
    chmod -R o-rwx /data/${CLEANDOMAIN}
    chmod -R og-rwx /data/${CLEANDOMAIN}/.ssh/
}

function supervision() {
    echo "command[check_bkp_${CLEANDOMAIN}]=/usr/bin/sudo /usr/lib/nagios/plugins/check_backup_file -d /data/${CLEANDOMAIN}/borg/$(echo ${SERVERNAME} | sed 's+\..*++g')/ -t 25 -T 49" >> /etc/nagios/nrpe_local.cfg
    systemctl restart nagios-nrpe-server
}

function rsa_key() {
    echo "# Please paste and save RSA key..." >> /data/${CLEANDOMAIN}/.ssh/authorized_keys
    nano /data/${CLEANDOMAIN}/.ssh/authorized_keys
}

main "$@"

#!/bin/bash
FILES=$1
ERROR=$2

sed -n 's#^.*time\":\"\(.*\)T.*user\":\"\(.*\)\",\"app.*Expected filesize.*mirall/\(.*\)stable.*$#\1 \2_(nextcloud_\3)#p' $FILES > $FILES.sort
#sed -n "s#^.*time\":\"\(.*\)T.*user\":\"\(.*\)\",\"app.*${ERROR}.*mirall/\(.*\)stable.*$#\1 \2_(nextcloud_\3)#p" $FILES > $FILES.sort

for b in $(awk '{print $1}' $FILES.sort|sort|uniq)
do echo $b
grep $b $FILES.sort|awk '{print $2}'|sort|uniq -c 
done

rm $FILES.sort

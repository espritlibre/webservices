#!/bin/bash

DATABASE="TO_DEFINE"
EXPORTPATH="/path/to/nextcloud/user/files"
USERFILES="/short/path/for/user/files/in/nextcloud"
WWWPATH="/path/to/nextcloud/www"
DATETODAY="$(date +%F)"

if [ ${DATETODAY:5:2} -eq "01" ]; then
  DATEPASTMONTH="$((${DATETODAY:0:4}-1))-12-01"
else
  MONTH=$(expr ${DATETODAY:5:2} - 1)
  DATEPASTMONTH="$(date +%Y)-$(printf %02d ${MONTH})-01"
fi

mysql -D "${DATABASE}" -e "select CAST(FROM_UNIXTIME(timestamp) as date) as Date,subject as Opération,user as Qui,file as Fichier from oc_activity where type not like 'calendar%' and type!='personal_settings' and type!='favorite' and subject not like '%by' and timestamp>=UNIX_TIMESTAMP('$DATEPASTMONTH') and timestamp<UNIX_TIMESTAMP('$DATETODAY') order by Date desc;" > "$EXPORTPATH"/export_"$DATETODAY".csv

chown -R www-data: "${EXPORTPATH}"
cd "${WWWPATH}" || exit
sudo -u www-data php -f ./occ files:scan --path="${USERFILES}"

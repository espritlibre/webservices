#!/bin/bash

# Script to launch on Nagios server

function main() {
    readvars "$@"

    service_monitoring

    backup_monitoring

    # Si besoin de tester la config Nagios :
    # /usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg
    systemctl restart nagios
}

function readvars() {
    while [[ $# -gt 0 ]]
    do
    key="$1"

    case $key in
        -d|--domain)
            if [ -w "$2" ]; then
                DOMAIN="${2}"
            else
                echo "[ERROR] Invalid domain : $2"
                exit 1
            fi
            shift
            ;;
        -h|--host)
            if [ -w "$2" ]; then
                SERVERNAME="${2}"
            else
                echo "[WARNING] No server name to configure backup : $2"
            fi
            shift
            ;;
        -b|--backup)
            if [ -w "$2" ]; then
                BKPSERVER="${2}"
            else
                echo "[WARNING] No backup server name to configure backup : $2"
            fi
            shift
            ;;
        *)
            echo "[WARNING] Unkown option: $1"
            ;;
    esac
    shift
    done

	if [ -z "${DOMAIN}" ]; then
		echo '[ERROR] No valid domain.'
		exit 1
    else
        CLEANDOMAIN=$(echo ${DOMAIN} | sed 's,https*://,,g' | sed 's,/,,g' | sed 's,\.,,g' | sed 's,-,,g')
	fi

    return 0
}

function service_monitoring() {
    echo "define service{" >> /usr/local/nagios/etc/servers/${SERVERNAME}.cfg
    echo "        use                     generic-service" >> /usr/local/nagios/etc/servers/${SERVERNAME}.cfg
    echo "        host_name               ${SERVERNAME}" >> /usr/local/nagios/etc/servers/${SERVERNAME}.cfg
    echo "        service_description     Check ${DOMAIN}" >> /usr/local/nagios/etc/servers/${SERVERNAME}.cfg
    echo "        servicegroups           web-services" >> /usr/local/nagios/etc/servers/${SERVERNAME}.cfg
    echo "        check_command           check_nrpe\!check_ssl_cert_${CLEANDOMAIN}" >> /usr/local/nagios/etc/servers/${SERVERNAME}.cfg
    echo "        check_interval          1440 #min" >> /usr/local/nagios/etc/servers/${SERVERNAME}.cfg
    echo "        notification_interval   0 #notify once" >> /usr/local/nagios/etc/servers/${SERVERNAME}.cfg
    echo "        notification_period     workhours" >> /usr/local/nagios/etc/servers/${SERVERNAME}.cfg
    echo "        }" >> /usr/local/nagios/etc/servers/${SERVERNAME}.cfg
    sed -i 's+\\!+!+g' /usr/local/nagios/etc/servers/${SERVERNAME}.cfg
}

function backup_monitoring() {
    echo "define service{" >> /usr/local/nagios/etc/servers/${BKPSERVER}.cfg
    echo "        use                     generic-service" >> /usr/local/nagios/etc/servers/${BKPSERVER}.cfg
    echo "        host_name               ${BKPSERVER}" >> /usr/local/nagios/etc/servers/${BKPSERVER}.cfg
    echo "        service_description     Borg ${DOMAIN}" >> /usr/local/nagios/etc/servers/${BKPSERVER}.cfg
    echo "        servicegroups           data-services" >> /usr/local/nagios/etc/servers/${BKPSERVER}.cfg
    echo "        check_command           check_nrpe_v2\!check_bkp_${CLEANDOMAIN}" >> /usr/local/nagios/etc/servers/${BKPSERVER}.cfg
    echo "        check_interval          720 #min" >> /usr/local/nagios/etc/servers/${BKPSERVER}.cfg
    echo "        notification_interval   0 #notify once" >> /usr/local/nagios/etc/servers/${BKPSERVER}.cfg
    echo "        notification_period     workhours" >> /usr/local/nagios/etc/servers/${BKPSERVER}.cfg
    echo "        }" >> /usr/local/nagios/etc/servers/${BKPSERVER}.cfg
    sed -i 's+\\!+!+g' /usr/local/nagios/etc/servers/${BKPSERVER}.cfg
}

main "$@"
